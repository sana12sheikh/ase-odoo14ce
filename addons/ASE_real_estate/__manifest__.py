# -*- coding: utf-8 -*-
{
    'name': "ASE_real_estate",

    'summary': """
        Real Estate Solution by Sana Khurram""",

    'description': """
        Real Estate Solution by Sana Khurram
    """,

    'author': "My Company",
    'website': "http://www.sanakhurram.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','contacts','account'],

    # always loaded
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/real_estate_views.xml',
        'views/templates.xml',
        # 'views/member.xml',
    ],
    'qweb': [
        'static/src/xml/partner_autocomplete.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
